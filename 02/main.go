package main

import (
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/JackBracken/aoc2018/std"
)

func uniqueCharacters(s string) []string {
	chars := strings.Split(s, "")
	sort.Strings(chars)

	j := 0
	for i := 1; i < len(chars); i++ {
		if chars[j] == chars[i] {
			continue
		}
		j++
		chars[i], chars[j] = chars[j], chars[i]
	}

	return chars[:j+1]
}

func hasNOccurences(s string, n int) bool {
	for _, c := range uniqueCharacters(s) {
		if strings.Count(s, c) == n {
			return true
		}
	}

	return false
}

func part1() {
	doubles, triples := 0, 0

	for _, v := range std.LoadFile() {
		if hasNOccurences(v, 2) {
			doubles++
		}
		if hasNOccurences(v, 3) {
			triples++
		}
	}

	fmt.Println(doubles * triples)
}

func similar(a, b string) bool {
	distance := 0
	for i := range a {
		if a[i] != b[i] {
			distance++
		}
	}
	return distance == 1
}

func printEqualChars(a, b string) {
	for i := range a {
		if a[i] == b[i] {
			fmt.Print(string(a[i]))
		}
	}
}

func part2() {
	in := std.LoadFile()

	for i, v := range in {
		for j := i + 1; j < (len(in) - 1); j++ {
			if similar(v, in[j]) {
				printEqualChars(v, in[j])
				os.Exit(0)
			}
		}
	}
}

func main() {
	part1()
	part2()
}
