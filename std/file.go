package std

import (
	"bufio"
	"log"
	"os"
)

var (
	// FileName is the default inputs filename. This should be placed
	// with the go files in each day directory.
	FileName = "input.txt"
)

// LoadFile returns each line of FileName in an array of string
func LoadFile() []string {
	var lines []string

	file, err := os.Open(FileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return lines
}
