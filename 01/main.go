package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/JackBracken/aoc2018/std"
)

func main() {
	freq, i := 0, 0

	var visited = make(map[int]bool)
	visited[freq] = true

	seq := std.LoadFile()

	for {
		if i >= len(seq) {
			i = 0
		}

		line := seq[i]
		amount, err := strconv.Atoi(line[1:])
		if err != nil {
			log.Fatal(err)
		}

		if line[0] == '+' {
			freq = freq + amount
		} else if line[0] == '-' {
			freq = freq - amount
		} else {
			log.Fatal("unexpected input", line)
		}

		if visited[freq] {
			fmt.Println(freq)
			os.Exit(0)
		}
		visited[freq] = true

		i++
	}
}
